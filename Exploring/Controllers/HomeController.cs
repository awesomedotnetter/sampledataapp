﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PagedList;

namespace Exploring.Controllers
{
    public class DataViewModel
    {
        public int number { get; set; }
        public string value { get; set; }
        public List<int> numberList { get; set; }
        public IPagedList<int> PagedList { get; set; }
    }


    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
        
            return View();
        }

        [HttpPost()]
        public ActionResult PostAction(DataViewModel model, int ? page)
        {
            var numberList = Enumerable.Range(1, model.number);

            //foreach (int number in numberList)
            //{
            //    if ((number % 3 == 0) && (number % 5 == 0))
            //        Console.WriteLine(string.Format("{0} is divisble by both 3 and 5", number));
            //    else if (number % 3 == 0)
            //        Console.WriteLine(string.Format("{0} is divisible by 3", number));
            //    else if (number % 5 == 0)
            //        Console.WriteLine(string.Format("{0} is divisible by 5", number));
            //}

            model.numberList = numberList.ToList();

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            model.PagedList = model.numberList.ToPagedList(pageNumber, pageSize);
            
            return View("Index",model);
        }
    }
}
